import React, { Component } from 'react';
import {BrowserRouter,Switch,Route} from 'react-router-dom';
import Index from './component';
import 'element-theme-default';import { i18n} from 'element-react'
import locale from 'element-react/src/locale/lang/vi'

i18n.use(locale);


class App extends Component {
  state = {  }
  render() { 
    return (  
      <BrowserRouter>
      <div>
        <Switch>
          <Route exact path="/" render={()=><Index/>}  />
        </Switch>
      </div>
  </BrowserRouter>
    );
  }
}
 
export default App;
