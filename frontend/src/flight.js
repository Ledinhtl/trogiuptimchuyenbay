import React, { Component } from 'react';
import './css/flight.css'
import line from './images/border.png'
class Flight extends Component {
    state = {}

    getDay(string) {
        let dateTime = new Date(string);
        let date = dateTime.getDate();
        let month = dateTime.getMonth();
        let year = dateTime.getFullYear();
        let day = date + '/' + month + '/' + year;
        return day;
    }
    getTime(string) {
        let dateTime = new Date(string);
        let hour = dateTime.getHours();
        let min = dateTime.getMinutes();
        if (hour < 10) hour = '0' + hour;
        if (min < 10) min = '0' + min;
        let time = hour + ':' + min;
        return time;
    }
    render() {
        let data = this.props.data;
        let giaiTri, congSac, suatAn, hanhLyThem;
        if (data.hanh_ly_ky_gui !== '')
            hanhLyThem = <div>
                <span className='icon'>
                    <i className="fa fa-suitcase" aria-hidden="true"></i>
                </span>
                Hành lý x {data.hanh_ly_ky_gui} kg</div>
        if (data.giai_tri === '1')
            giaiTri = <div>
                <span className='icon'>
                    <i className="fa fa-gamepad" aria-hidden="true"></i></span>
                Giải trí</div>
        if (data.suat_an === '1')
            suatAn = <div>
                <span className='icon'><i className="fa fa-cutlery" aria-hidden="true"> </i></span>
                Suất ăn
                </div>
        if (data.cong_sac === '1')
            congSac = <div>
                <span className='icon'><i className="fa fa-battery-full" aria-hidden="true"></i> </span>
                Cổng sạc</div>
        return (
            <div className='flight-detail'>
                <div className='left-content'>
                    <div><img width='auto' height='20vh' src={'http://localhost:5000'+data.logo_hang} alt=''/></div>
                    <span className='bold'>{data.ten_chuyen_bay}</span>
                </div>
                <div className='middle-content'>
                    <div className='md-ct-left'>
                        <img src={line} alt='l' />
                        {/* <i className="fa fa-long-arrow-down fa-5x " aria-hidden="true"></i> */}
                    </div>
                    <div className='middle-row'>
                        {/* <i className="fa fa-circle-thin" aria-hidden="true"></i> */}
                        <div className='cl-time'>
                            <div>
                                <span className='bold'>{this.getTime(data.ngay_gio_di)}</span>
                            </div>
                            <div>
                                {this.getDay(data.ngay_gio_di)}
                            </div>
                        </div>
                        <div className='cl-airport'>
                            <div>
                                <div><span className='bold'>{data.tinh_di}</span></div>
                                <div>{data.ten_san_bay_di}</div>
                            </div>
                        </div>
                    </div>
                    <div className='middle-row'>
                        <div className='cl-time'>
                            <div>
                                <span className='bold'>{this.getTime(data.ngay_gio_den)}</span>
                            </div>
                            <div>
                                {this.getDay(data.ngay_gio_den)}
                            </div>
                        </div>
                        <div>
                            <div><span className='bold'>{data.tinh_den} </span></div>
                            <div>{data.ten_san_bay_den}</div>
                        </div>
                    </div>
                </div>
                <div className='right-content'>
                    <div><span className='bold'>{'Máy bay  '}</span> {data.may_bay}</div>
                    <div><span className='bold'>{'Khoảng cách ghế  '}</span> {data.khoang_cach_ghe}</div>
                    <div><span className='bold'>{'Sơ đồ ghế ngồi  '}</span> {data.so_do_ghe_ngoi}</div>
                    <div><span className='bold'>{'Hành lý xách tay  '}</span>{data.hanh_ly_xach_tay} kg</div>
                    {hanhLyThem}
                    {giaiTri}
                    {suatAn}
                    {congSac}
                </div>
            </div>
        );
    }
}

export default Flight;