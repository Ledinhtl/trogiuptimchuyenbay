import React, { Component } from 'react';
import SearchForm from './search-form';
import FlightList from './list-flights';

class Index extends Component {
    state = { 
        flights:[],
     }
    onGetData=(flights,listPassengers)=>{
        // this.setState({flights:flights,listPassengers:listPassengers})
        this.setState({flights:flights})
    }
    render() { 
        return ( 
            <div>
                <SearchForm onGetData={this.onGetData}/>
                <FlightList flights={this.state.flights} listPassengers={this.state.listPassengers}/>
            </div>
         );
    }
}
 
export default Index;