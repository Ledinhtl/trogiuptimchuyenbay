import React, { Component } from 'react';

class Ticket extends Component {
    state = {}
    render() {
        let flight = this.props.data;
        let price = 0;
        flight.map(x => price=price+x.gia_ve);
        let type = this.props.type;
        let listPassengers = { adult: 1, child: 0, baby: 0 }
        let js = JSON.parse(localStorage.getItem('psg'));
        if (js != null) listPassengers = js
        let child = parseInt(listPassengers.child);
        let baby = parseInt(listPassengers.baby);
        let childRow, babyRow;
        if (child > 0) childRow = (
            <tr>
                <td className='cl-1-pr'>{'Vé trẻ em cơ bản (x' + child + ')'}</td>
                <td className='td-price'>{Math.round(price*0.75*child)} VNĐ</td>
            </tr>
        )
        if (baby > 0) babyRow = (
            <tr>
                <td className='cl-1-pr'>{'Vé em bé cơ bản (x' + baby + ')'}</td>
                <td className='td-price'>{Math.round(price *0.1*baby)} VNĐ</td>
            </tr>
        )
        
        return (
            <div>
                <div className='left-content'>
                    {flight[0].tinh_di + ' (' + flight[0].ma_san_bay_di + ') - ' + flight[type].tinh_den
                        + ' (' + flight[type].ma_san_bay_den + ') '}
                </div>
                <div className='right-ct-tic'>
                    <table>
                        <tr>
                            <td className='cl-1-pr'>{'Vé người lớn cơ bản (x' + listPassengers.adult + ')'}</td>
                            <td className='td-price' >{price*listPassengers.adult} VNĐ</td>
                        </tr>
                        {childRow}
                        {babyRow}
                        <tr id='total-row'>
                            <td className='cl-1-pr'>Tổng cộng</td>
                            <td className='td-price'>{Math.round(price*0.75*child)+price*listPassengers.adult
                            +Math.round(price *0.1*baby)} VNĐ</td>
                        </tr>
                    </table>


                </div>
            </div>
        );
    }
}

export default Ticket;