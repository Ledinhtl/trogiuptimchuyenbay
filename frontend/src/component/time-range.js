import React, { Component } from 'react';
import { TimeRangePicker } from 'element-react';

class TimeRange extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: [new Date(2016, 9, 10, 0, 30), new Date(2016, 9, 10, 12, 0)]
        }
    }

    handleUpdate(value) {
        this.props.onTimeRangeChange(value)
        // console.debug('time-picker update: ', value)
    }

    render() {
        return (
            <TimeRangePicker
                // pickerWidth={500}
                onChange={this.handleUpdate.bind(this)}
                placeholder="Pick a time range"
                value={this.state.value}
                className='time-ran-pic'
            />
        )
    }

}

export default TimeRange;