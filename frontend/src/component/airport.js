import React, { Component } from 'react';
import { Select } from 'element-react';
import Axios from 'axios'

class Airport extends Component {
    constructor(props) {
        super(props);

        this.state = {
            options: [],
            airports: []
        }
    }
    
    onChange=(airport)=>{
        console.log(airport);
        this.props.onChangeAirport(airport,this.props.type)
    }
    async onSearch(query) {
        if (query !== '') {
            this.setState({
                loading: true
            });
            await Axios.get('http://127.0.0.1:5000/airportsearch/'+query)
            .then(rsp => {
            // console.log(rsp)
            this.setState({loading: false});
            this.setState({airports: rsp.data,
                options: rsp.data.map(item => {
                    return { value: item, label: item.ten_tinh+', '+item.ten_nuoc+' - '+item.ten+'('+item.ma+')' };
                }).filter(item => {
                    return item.label.toLowerCase().indexOf(query.toLowerCase()) > -1;
                })
            })
        })

            // setTimeout(() => {
            //     this.setState({
            //         loading: false,
            //         options: this.state.airports.map(item => {
            //             return { value: item, label: item.ten_tinh+', '+item.ten_nuoc+' - '+item.ten+'('+item.ma+')' };
            //         }).filter(item => {
            //             return item.label.toLowerCase().indexOf(query.toLowerCase()) > -1;
            //         })
            //     });
            // }, 200);
        } else {
            this.setState({
                options: []
            });
        }
    }

    render() {
        return (
            <Select className='sl-airp' placeholder='Chọn sân bay'value={this.state.value} multiple={false} 
            filterable={true} remote={true} noDataText='Vui lòng nhập từ khoá' noMatchText='Không có kết quả phù hợp'
            remoteMethod={this.onSearch.bind(this)} loading={this.state.loading} onChange={this.onChange}>
                {
                    this.state.options.map(el => {
                        return <Select.Option key={el.value} label={el.label} value={el.value} />
                    })
                }
            </Select>
        )
    }
}

export default Airport;