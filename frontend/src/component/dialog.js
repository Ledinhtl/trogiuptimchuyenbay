import React, { Component } from 'react';
import { Form, Button, Input } from 'element-react';

class DialogContent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            form: {
                adult: 1,
                child: 0,
                baby: 0
            }
        };
    }

    submit=()=> {
        let form = this.state.form;
        this.props.onSubmitDialog(form.adult, form.child, form.baby);
    }

    onChange(key, value) {
        this.state.form[key] = value;
        this.forceUpdate();
    }

    render() {
        return (
            <div className='dl'>
                <table>
                    <tr>
                        <td width='50%'>Người lớn</td>
                        <td>
                            <Input type='number' value={this.state.form.adult} min='1' 
                            onChange={this.onChange.bind(this, 'adult')}></Input>
                        </td>
                    </tr>
                    <tr>
                        <td>Trẻ em</td>
                        <td>
                            <Input type='number' value={this.state.form.child} min='0' 
                            onChange={this.onChange.bind(this, 'child')}></Input>
                        </td>
                    </tr>
                    <tr>
                        <td>Em bé</td>
                        <td>
                            <Input type='number' value={this.state.form.baby} min='0' 
                            onChange={this.onChange.bind(this, 'baby')}></Input>
                        </td>
                    </tr>
                </table>
                <div className='dl-bt'>
                <Button  type="primary" onClick={this.submit}>OK</Button></div>
            </div>
        )
    }

}

export default DialogContent;