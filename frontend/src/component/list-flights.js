import React, { Component } from 'react';
import { Table, Tabs, Tag, Select } from 'element-react';
import Flight from '../flight';
import Ticket from './tiket';

class FlightList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            columns: [
                {
                    type: 'expand',
                    expandPannel: function (data) {
                        let flight;
                        flight = data.flight.map(x => <Flight data={x} key={x.gia_ve} />);
                        return (
                            <Tabs activeName="1" onTabClick={(tab) => console.log(tab.props.name)}>
                                <Tabs.Pane label="Chi tiết chuyến bay" name="1">{flight}</Tabs.Pane>
                                <Tabs.Pane label="Chi tiết vé" name="2"><Ticket data={data.flight} type={data.type} /></Tabs.Pane>
                            </Tabs>

                        )
                    }
                },
                {
                    label: "Hãng ",
                    width:150,
                    render: function (data) {
                        let brand=[];
                        for(let i=0;i<data.flight.length;i++) 
                            if(brand.indexOf(data.flight[i].ten_hang)<0) brand.push(data.flight[i].ten_hang)
                        if (brand.length>=2) return 'Nhiều hãng'
                        return data.flight[0].ten_hang;
                    }
                }, {
                    label: "Điểm đi",
                    render: function (data) {
                        return data.flight[0].tinh_di;
                    }
                }, {
                    label: "Điểm đến",
                    render: function (data) {
                        return data.flight[data.type].tinh_den;
                    }
                }, {
                    label: "Giờ khởi hành",
                    render: function (data) {
                        let dateTime = new Date(data.flight[0].ngay_gio_di);
                        let hour = dateTime.getHours(); if (hour < 10) hour = '0' + hour;
                        let min = dateTime.getMinutes(); if (min < 10) min = '0' + min;
                        return (
                            <span>
                                {hour + ':' + min}
                            </span>)
                    }
                }, {
                    label: "Giờ đến",
                    prop: "flight.ngay_gio_den",
                    render: function (data) {
                        let dateTime = new Date(data.flight[data.type].ngay_gio_den);
                        let di = new Date(data.flight[0].ngay_gio_di);
                        let totalMins = (dateTime - di) / 1000 / 60;
                        let day = Math.floor(totalMins / 60 / 24);
                        let hour = dateTime.getHours(); if (hour < 10) hour = '0' + hour;
                        let min = dateTime.getMinutes(); if (min < 10) min = '0' + min;
                        let more = '';
                        if (day > 0) more = '(+' + day + 'd)';
                        return (
                            <span>
                                {hour + ':' + min + more}
                            </span>)
                    }
                }, {
                    label: 'Điểm dừng',
                    prop: 'type',
                    filters: [{ text: 'Bay thẳng', value: '0' }, { text: '1 điểm dừng', value: '1' },
                    { text: '2+ điểm dừng', value: '2' }],
                    filterMethod(value, row) {
                        if (value === '2') return row.type >= value;
                        return row.type == value;
                    },
                    render: (data) => {
                        return (data.type);
                    }
                }, {
                    label: "Thời gian bay",
                    render: function (data) {
                        let den = new Date(data.flight[data.type].ngay_gio_den);
                        let di = new Date(data.flight[0].ngay_gio_di);
                        let totalMins = (den - di) / 1000 / 60;
                        let hour = Math.floor(totalMins / 60);
                        let min = Math.floor(totalMins - hour * 60);
                        return hour + 'h ' + min + 'm';
                    }
                }, {
                    label: "Dịch vụ",
                    filters: [{ text: 'Suất ăn', value: 0 }, { text: 'Giải trí', value: 1 },
                    { text: 'Cổng sạc', value: 2 }],
                    filterMethod(value, row) {
                        if (value === 0)
                            return row.flight[0].suat_an == '1';
                        if (value === 1)
                            return row.flight[0].giai_tri == '1';
                        if (value === 2)
                            return row.flight[0].cong_sac == '1';
                    },
                    render: function (data) {
                        let hanhLy, suatAn, giaiTri, congSac, total;
                        for (let i = 0; i < data.flight.length; i++) {
                            if (data.flight[i].hanh_ly_ky_gui != '')
                                hanhLy = <i className="fa fa-suitcase" aria-hidden="true"></i>
                            if (data.flight[i].suat_an === '1')
                                suatAn = <i className="fa fa-cutlery" aria-hidden="true"></i>
                            if (data.flight[i].giai_tri === '1')
                                giaiTri = <i className="fa fa-gamepad" aria-hidden="true"></i>
                            if (data.flight[i].cong_sac === '1')
                                congSac = <i className="fa fa-battery-full" aria-hidden="true"></i>
                        }
                        return (<>{hanhLy} {suatAn} {giaiTri} {congSac}</>)
                    }
                }, {
                    label: "Giá tiền",
                    sortable: true,
                    sortMethod: function (data1,data2) {
                        let price1 = 0,price2=0;
                        data1.flight.map(x => price1 = price1+ x.gia_ve)
                        data2.flight.map(x => price2 = price2+ x.gia_ve)
                        if(price1-price2 <0) return false; 
                        return true;
                    },
                    render: function (data) {
                        let price = 0;
                        data.flight.map(x => price = price + x.gia_ve)
                        return (
                            <span>
                                {price + ' VNĐ'}
                            </span>)
                    }
                }
            ],
            data: [
                {
                    "type": 2,
                    "flight": [
                        {
                            "tinh_di": "Hà Nội",
                            "nuoc_di": "Việt Nam",
                            "ten_san_bay_di": "Sân bay Nội Bài ",
                            "ma_san_bay_di": "HAN",
                            "tinh_den": "Đà Nẵng",
                            "nuoc_den": "Việt Nam",
                            "ten_san_bay_den": "Sân bay Đà Nẵng",
                            "ma_san_bay_den": "DAD",
                            "ten_hang": "Vietnam Airlines",
                            "logo_hang": "/images/vietnam_airlines.png",
                            "hanh_ly_xach_tay": "12",
                            "hanh_ly_ky_gui": "23",
                            "ten_chuyen_bay": "Vietnam VN-155",
                            "ngay_gio_di": "2019-12-24T23:00:00.000Z",
                            "ngay_gio_den": "2019-12-25T00:20:00.000Z",
                            "thoi_gian_bay": "01:20:00",
                            "gia_ve": 858713,
                            "may_bay": "Airbus A321",
                            "so_do_ghe_ngoi": "03-03",
                            "khoang_cach_ghe": "31 inch (trên tiêu chuẩn)",
                            "suat_an": "1",
                            "giai_tri": "1",
                            "cong_sac": "1",
                            "loai_ve": "",
                            "max_con_time": 6,
                            "time_forward": 5
                        },
                        {
                            "tinh_di": "Đà Nẵng",
                            "nuoc_di": "Việt Nam",
                            "ten_san_bay_di": "Sân bay Đà Nẵng",
                            "ma_san_bay_di": "DAD",
                            "tinh_den": "TP HCM",
                            "nuoc_den": "Việt Nam",
                            "ten_san_bay_den": "Sân bay Tân Sơn Nhất",
                            "ma_san_bay_den": "SGN",
                            "ten_hang": "Vietnam Airlines",
                            "logo_hang": "/images/vietnam_airlines.png",
                            "hanh_ly_xach_tay": "12",
                            "hanh_ly_ky_gui": "23",
                            "ten_chuyen_bay": "Vietnam VN-125",
                            "ngay_gio_di": "2019-12-25T06:00:00.000Z",
                            "ngay_gio_den": "2019-12-25T07:30:00.000Z",
                            "thoi_gian_bay": "01:30:00",
                            "gia_ve": 1410189,
                            "may_bay": "Airbus A321",
                            "so_do_ghe_ngoi": "03-03",
                            "khoang_cach_ghe": "31 inch (trên tiêu chuẩn)",
                            "suat_an": "",
                            "giai_tri": "1",
                            "cong_sac": "",
                            "loai_ve": "",
                            "max_con_time": 6,
                            "time_forward": 5
                        },
                        {
                            "tinh_di": "TP HCM",
                            "nuoc_di": "Việt Nam",
                            "ten_san_bay_di": "Sân bay Tân Sơn Nhất",
                            "ma_san_bay_di": "SGN",
                            "tinh_den": "Nha Trang",
                            "nuoc_den": "Việt Nam",
                            "ten_san_bay_den": "Sân bay Cam Ranh",
                            "ma_san_bay_den": "CXR",
                            "ten_hang": "VietJet Air",
                            "logo_hang": "/images/vietjet_air.png",
                            "hanh_ly_xach_tay": "7",
                            "hanh_ly_ky_gui": "",
                            "ten_chuyen_bay": "VietJet Air VJ-690",
                            "ngay_gio_di": "2019-12-25T13:00:00.000Z",
                            "ngay_gio_den": "2019-12-25T14:00:00.000Z",
                            "thoi_gian_bay": "01:00:00",
                            "gia_ve": 834025,
                            "may_bay": "Airbus A321",
                            "so_do_ghe_ngoi": "3-3",
                            "khoang_cach_ghe": "28 inch (tiêu chuẩn)",
                            "suat_an": null,
                            "giai_tri": null,
                            "cong_sac": null,
                            "loai_ve": null
                        }
                    ],
                    "score": 0.272830458696345
                },
                {
                    "type": 1,
                    "flight": [
                        {
                            "tinh_di": "Hà Nội",
                            "nuoc_di": "Việt Nam",
                            "ten_san_bay_di": "Sân bay Nội Bài ",
                            "ma_san_bay_di": "HAN",
                            "tinh_den": "TP HCM",
                            "nuoc_den": "Việt Nam",
                            "ten_san_bay_den": "Sân bay Tân Sơn Nhất",
                            "ma_san_bay_den": "SGN",
                            "ten_hang": "Jetstar",
                            "logo_hang": "/images/jetstar.png",
                            "hanh_ly_xach_tay": "7",
                            "hanh_ly_ky_gui": "",
                            "ten_chuyen_bay": "Jetstar BL-765",
                            "ngay_gio_di": "2019-12-25T05:05:00.000Z",
                            "ngay_gio_den": "2019-12-25T07:20:00.000Z",
                            "thoi_gian_bay": "02:15:00",
                            "gia_ve": 1385184,
                            "may_bay": "Airbus A320",
                            "so_do_ghe_ngoi": "03-03",
                            "khoang_cach_ghe": "29 inch (tiêu chuẩn)",
                            "suat_an": "",
                            "giai_tri": "",
                            "cong_sac": "",
                            "loai_ve": "",
                            "max_con_time": 6,
                            "time_forward": 5
                        },
                        {
                            "tinh_di": "TP HCM",
                            "nuoc_di": "Việt Nam",
                            "ten_san_bay_di": "Sân bay Tân Sơn Nhất",
                            "ma_san_bay_di": "SGN",
                            "tinh_den": "Nha Trang",
                            "nuoc_den": "Việt Nam",
                            "ten_san_bay_den": "Sân bay Cam Ranh",
                            "ma_san_bay_den": "CXR",
                            "ten_hang": "VietJet Air",
                            "logo_hang": "/images/vietjet_air.png",
                            "hanh_ly_xach_tay": "7",
                            "hanh_ly_ky_gui": "",
                            "ten_chuyen_bay": "VietJet Air VJ-690",
                            "ngay_gio_di": "2019-12-25T13:00:00.000Z",
                            "ngay_gio_den": "2019-12-25T14:00:00.000Z",
                            "thoi_gian_bay": "01:00:00",
                            "gia_ve": 834025,
                            "may_bay": "Airbus A321",
                            "so_do_ghe_ngoi": "3-3",
                            "khoang_cach_ghe": "28 inch (tiêu chuẩn)",
                            "suat_an": null,
                            "giai_tri": null,
                            "cong_sac": null,
                            "loai_ve": null
                        }
                    ],
                    "score": 0.2205422577373952
                },
            ], value: [],listData:[]
        }
    }
    onChangeAirpotFilter=(airports)=>{
        if (airports.length>0){
            let data=this.props.flights,list=[];
            data.map(x => {
                x.flight.map(y => {
                    console.log(y.ten_hang)
                    if (airports.indexOf(y.ten_hang) >= 0 ) list.push(x)
                })
            })
            this.setState({listData:list})
        }else this.setState({listData:[]})
    }
    render() {
        let data = this.props.flights;
        let brand = [], airportFilter;
        if (data.length > 0) {
            data.map(x => {
                x.flight.map(y => {
                    if (brand.indexOf(y.ten_hang) < 0) brand.push(y.ten_hang)
                })
            })
        }
        if (brand.length > 0) airportFilter = (
            <div id='brand-filter'>
            <Select className='brand-select' value={this.state.value} placeholder='Chọn hãng hàng không' multiple onChange={this.onChangeAirpotFilter}> 
                {
                    brand.map(el => {
                        return (
                          <Select.Option key={el} label={el} value={el}>
                            <span>{el}</span>
                          </Select.Option>
                        )
                      })
                }
            </Select>
            </div>
        )
        let listRender;
        if(this.state.listData.length===0)listRender=data;else listRender=this.state.listData;
        return (
            <div className='result'>
                {airportFilter}
                <Table
                    style={{ width: '92%', marginLeft: '4%' }}
                    columns={this.state.columns}
                    data={listRender}
                    // data={this.state.data}
                    border={false}
                    onCurrentChange={item => { console.log(item) }}
                    defaultExpandAll={true}
                    emptyText='Không có kết quả phù hợp'
                />
            </div>
        )
    }
}

export default FlightList;