import React, { Component } from 'react';
import { Button, Checkbox, Form, DatePicker, Layout, Input, Switch, Select, TimePicker, Dropdown, Slider, Dialog, Message } from 'element-react';
import Airport from './airport';
import Axios from 'axios';
import '../css/search-form.css'
import DialogContent from './dialog';
import ReactSlider from 'react-slider'
import TimeRange from './time-range';
import { min } from 'moment';
class SearchForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            to: '',
            from: '',
            form: {
                date1: null,
                date2: null,
            },
            passengers: '',
            listPassengers: {
                adult: 0,
                child: 0,
                baby: 0
            },
            timeRange:[new Date(2016, 9, 10, 0, 30), new Date(2016, 9, 10, 12, 0)]
        };
    }
    // componentDidMount() {
    //     Axios.get('http://127.0.0.1:5000/flightsearch?from=PQC&to=SGN&date=2019-12-25&max_con_time=12&min_con_time=5&time=12:20')
    //         .then(rsp => {
    //             // console.log(rsp)
    //             this.props.onGetData(rsp.data,this.state.listPassengers)
    //         })
    // }
    
    onSubmit(e) {
        e.preventDefault();
        // console.log(this.state.form.date1+' '+this.state.form.date2)
        let from = this.state.from;
        let slider = this.state.form.slider; console.log(slider);
        let to = this.state.to;
        let day = new Date(this.state.form.date1);
        let time = new Date(this.state.form.date2);
        console.log(this.state.timeRange)
        let min_con_time= new Date (this.state.timeRange[0]);
        let max_con_time= new Date (this.state.timeRange[1]);
        console.log(day)
        console.log(from)
        console.log(to)
        if (from !== '' && to !== '' && day.getTime() !== new Date(0).getTime()) {
            let formatDay = day.getFullYear() + '-' + (day.getMonth() + 1) + '-' + day.getDate();
            let formatTime = time.getHours() + ':' + time.getMinutes();
            let minT= min_con_time.getHours()+min_con_time.getMinutes()/60;
            let maxT= max_con_time.getHours()+max_con_time.getMinutes()/60;
            console.log(minT);
            console.log(maxT);
            let url_api = 'http://127.0.0.1:5000/flightsearch?from=' + from + '&to=' + to + '&date=' + formatDay
                + '&max_con_time='+maxT+'&min_con_time='+minT+'&time=';
            if (time.getTime() !== new Date(0).getTime())
                url_api = url_api + formatTime;
            // console.log(this.state.listPassengers.adult)
            // console.log(formatTime+' '+formatDay)
            Axios.get(url_api)
                .then(rsp => {
                    // console.log(rsp)
                    // console.log(this.state.listPassengers)
                    // let data={flights:rsp.data,list:this.state.listPassengers}
                    // console.log(data)
                    localStorage.setItem('psg', JSON.stringify(this.state.listPassengers))
                    this.props.onGetData(rsp.data)
                })
        } else Message.warning('Vui lòng điền đủ thông tin')
    }

    onChange(key, value) {
        this.state.form[key] = value;
        this.forceUpdate();
    }
    changeAirport = (airport, type) => {
        console.log(type)
        if (type === 'from') this.setState({ from: airport.ma })
        if (type === 'to') this.setState({ to: airport.ma })
    }
    timeRangeChange=(value)=>{
        this.setState({timeRange:value})
    }
    formatTooltip(val) {
        let hr = Math.floor(val / 60);
        let min = val - hr * 60;
        return hr + 'h' + min;
    }
    submitDialog = (adult, child, baby) => {
        this.setState({ dialogVisible: false })
        this.setState({ passengers: adult + ' người lớn, ' + child + ' trẻ em, ' + baby + ' em bé ' })
        this.setState({ listPassengers: { adult: adult, child: child, baby: baby } })
    }
    render() {
        return (
            <Form labelPosition='left'	className="en-US" model={this.state.form} labelWidth="120" onSubmit={this.onSubmit.bind(this)}>
                <div className='row-1'>
                    <Form.Item required label="Điểm khởi hành" className='row-1-from'>
                        <Airport type='from' onChangeAirport={this.changeAirport} />
                    </Form.Item>
                    <Form.Item required className='row-1-to' label="Điểm đến">
                        <Airport type='to' onChangeAirport={this.changeAirport} />
                    </Form.Item>
                </div>
                <div className='row-2'>
                    <Form.Item required label="Ngày đi">
                        <Layout.Col span="6">
                            <Form.Item required prop="date1" labelWidth="0px">
                                <DatePicker
                                    value={this.state.form.date1}
                                    placeholder="Pick a date"
                                    onChange={this.onChange.bind(this, 'date1')}
                                />
                            </Form.Item>
                        </Layout.Col>
                        <Layout.Col className="line" span="1">-</Layout.Col>
                        <Layout.Col span="6">
                            <Form.Item prop="date2" labelWidth="0px">
                                <TimePicker
                                    value={this.state.form.date2}
                                    placeholder="Pick a time"
                                    onChange={this.onChange.bind(this, 'date2')}
                                />
                            </Form.Item>
                        </Layout.Col>
                    </Form.Item>
                </div>
                <div className='fm-sli'>
                    <Form.Item required labelWidth="200" label="Thời gian quá cảnh (nếu có)">

                        {/* <Slider  className="slider-time"  onChange={(oldValue)=>this.onChange(oldValue)}
                            max={24 * 60} range={true} formatTooltip={this.formatTooltip.bind(this)} /> */}
                        {/* <ReactSlider
                            className="horizontal-slider"
                            thumbClassName="example-thumb"
                            trackClassName="example-track"
                            defaultValue={[0, 20]}
                            ariaLabel={['Lower thumb', 'Upper thumb']}
                            ariaValuetext={state => `Thumb value ${state.valueNow}`}
                            renderThumb={(props, state) => <div {...props}>{state.valueNow}</div>}
                            pearling
                            minDistance={10}
                        /> */}
                        <TimeRange onTimeRangeChange={this.timeRangeChange}/>
                    </Form.Item>
                </div>
                <div className='ip-pas'>
                    <Form.Item label='Số hành khách' required>
                        <Input value={this.state.passengers} append={<Button type="primary" icon="search"
                            onClick={() => this.setState({ dialogVisible: true })}>Chọn</Button>} />
                    </Form.Item>
                </div>
                <div className='bt-sub'>
                    <Form.Item>
                        <Button type="primary" nativeType="submit">Tìm kiếm</Button>
                    </Form.Item>
                </div>
                <Dialog
                    title="Chọn số lượng hành khách"
                    size="tiny"
                    visible={this.state.dialogVisible}
                    onCancel={() => this.setState({ dialogVisible: false })}
                    lockScroll={false}
                >
                    <Dialog.Body>
                        <DialogContent onSubmitDialog={this.submitDialog} />
                    </Dialog.Body>
                </Dialog>
            </Form>
        )
    }
}

export default SearchForm;