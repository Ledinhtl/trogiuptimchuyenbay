var db = require("../common/database")
var q = require("q")
var moment = require('moment')

var conn = db.getConnection();

function getFirstFights(data) { // Tìm chuyến bay đầu tiên
    var sql = `SELECT pf.ten_tinh as tinh_di, pf.ten_nuoc as nuoc_di, apf.ten as ten_san_bay_di, apf.ma as ma_san_bay_di,
            pt.ten_tinh as tinh_den, pt.ten_nuoc as nuoc_den, apt.ten as ten_san_bay_den, apt.ma as ma_san_bay_den,
            al.ten as ten_hang, logo as logo_hang, hanh_ly_xach_tay, hanh_ly_ky_gui, f.ten as ten_chuyen_bay, ngay_gio_di,
            ngay_gio_den, thoi_gian_bay, gia_ve, may_bay, so_do_ghe_ngoi, khoang_cach_ghe, suat_an, giai_tri, cong_sac, loai_ve
            FROM tinh as pf join san_bay as apf on pf.ma = apf.ma_tinh join 
            chuyen_bay as f on apf.ma = f.ma_diem_dau join san_bay as apt on f.ma_diem_den = apt.ma join tinh as pt on apt.ma_tinh = pt.ma
            join hang_hang_khong as al on id_hang_hang_khong = al.id
            WHERE ma_diem_dau = ? and ngay_gio_di like ?` 
    var defer = q.defer();
    conn.query(sql, [data.from, /* data.to, */ data.date + '%'], function (err, result) {
        if (err) {
            defer.reject(err)
        }
        else {
            defer.resolve(result)
        }
    })
    return defer.promise;
}

function getAfterFlights(data) { // Tìm chuyến bay thứ 2 hoặc thứ 3 trong các chuyến nối
    var sql = `SELECT pf.ten_tinh as tinh_di, pf.ten_nuoc as nuoc_di, apf.ten as ten_san_bay_di, apf.ma as ma_san_bay_di,
    pt.ten_tinh as tinh_den, pt.ten_nuoc as nuoc_den, apt.ten as ten_san_bay_den, apt.ma as ma_san_bay_den,
    al.ten as ten_hang, logo as logo_hang, hanh_ly_xach_tay, hanh_ly_ky_gui, f.ten as ten_chuyen_bay, ngay_gio_di,
    ngay_gio_den, thoi_gian_bay, gia_ve, may_bay, so_do_ghe_ngoi, khoang_cach_ghe, suat_an, giai_tri, cong_sac, loai_ve
    FROM tinh as pf join san_bay as apf on pf.ma = apf.ma_tinh join 
    chuyen_bay as f on apf.ma = f.ma_diem_dau join san_bay as apt on f.ma_diem_den = apt.ma join tinh as pt on apt.ma_tinh = pt.ma
    join hang_hang_khong as al on id_hang_hang_khong = al.id
    WHERE ma_diem_dau = ? and ngay_gio_di  between ? and ?`
    var defer = q.defer();
    conn.query(sql, [data.ma_san_bay_den, moment(data.ngay_gio_den).add(data.time_forward, 'h').format('YYYY-MM-DD HH:mm:ss'),
    moment(data.ngay_gio_den).add(data.max_con_time, 'h').format('YYYY-MM-DD HH:mm:ss')], function (err, result) {
        if (err) {
            defer.reject(err)
        }
        else {
            defer.resolve(result)
        }
    })
    return defer.promise;

}

module.exports = {
    getFirstFights: getFirstFights,
    getAfterFlights: getAfterFlights,
}