var db = require("../common/database")
var q = require("q")


var conn = db.getConnection();


function searchAirports(query) { // Tìm sân bay
    var sql = `SELECT a.ma, ten, ten_tinh, ten_nuoc 
        FROM san_bay as a join tinh as p on a.ma_tinh = p.ma 
        where a.ma like N? or ten like N? or ten_tinh like N? or ten_nuoc like N?`;
    var defer = q.defer();
    query = '%' + query + '%'
    conn.query(sql,[query, query, query, query], function (err, result) {
        if (err) {
            defer.reject(err)
        }
        else {
            defer.resolve(result)
        }
    })
    return defer.promise;
}


module.exports = {
    searchAirports: searchAirports
}