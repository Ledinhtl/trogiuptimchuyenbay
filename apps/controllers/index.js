var express = require("express");
const MAX_FLIGHT = 15
var router = express.Router();
var moment = require('moment')
var airport_md = require("../models/airport")
var flight_md = require("../models/flight")
var dSWithouStartTime = require('../common/decision-support-without-start-time')
var dSUseStartTime = require('../common/decision-support-use-start-time')
var q = require('q')
var cors = require('cors')

router.get("/airportsearch/:query", cors(), function (req, res) { // API tim san bay
    var params = req.params;
    var query = params.query;
    var data = airport_md.searchAirports(query)
    if (data) {
        data.then(function (airports) {
            res.json(airports)
        })/* .catch(function (err) {
            var data = {
                error: "Could not get airport",
                errorInfo: err
            }
            res.json(data)
        }) */
    }
})

router.get("/flightsearch", cors(), function (req, res) {
    //try {
        var params = req.query; // Lấy các tham số mà người dùng nhập vào
        var fullFlights = [] // Chứa các chuyến bay được nối đầy đủ
        var data = {
            from: params.from.trim(),
            to: params.to.trim(),
            date: params.date.trim(),
            max_con_time: params.max_con_time,
            min_con_time: params.min_con_time
        }
        if (data.from.length != 3 || data.to.length != 3 || data.date.length < 8 || data.date.length > 10) {
            // Có tham số truyền vào không đúng
            res.json({ error: "Đường dẫn không hợp lệ" })
        }
        else if (data.from == data.to) {
            res.json({ error: "Hãy chọn điểm đến khác điểm đi" })
        }
        else {
            var temp_list = [] // Chua cac chuyen bay thang chua den dich
            var temp_list1 = [] // Chua cac chuyen bay thu 2 cua chuyen bay 1 diem dung
            var temp_list2 = [] // Chua cac chuyen bay 1 diem dung
            var result = flight_md.getFirstFights(data) // các chuyến bay có điểm đầu = Điểm đầu và ngày = ngày.
            result.then(function (flights) { // flights là các chuyến bay thẳng.
                for(var i = 0; i < flights.length; i++) {
                    if (flights[i].ma_san_bay_den == data.to) { // Nếu đến đích cần đến
                        if (fullFlights.length <= MAX_FLIGHT)
                        fullFlights.push({ type: 0, flight: [flights[i]] }) // đẩy vào mảng chung
                    }
                    else { // tìm các chuyến bay chưa đến đích
                        flights[i].max_con_time = parseFloat(data.max_con_time)
                        flights[i].time_forward = parseFloat(data.min_con_time)
                        temp_list.push(flights[i])
                    }
                }
            }).then(function () {
                // Tìm các chuyến bay 1 điểm dừng
                return q.all(temp_list.map(flight_md.getAfterFlights))

            }).then(function(result) {
                for(var i = 0; i < temp_list.length; i++) {
                    for (var j = 0; j < result[i].length; j++) {
                        if (result[i][j].ma_san_bay_den == data.to) { // Nếu đến đích thì ghi nhận
                            if (fullFlights.length <= 2 * MAX_FLIGHT)
                            fullFlights.push({type: 1, flight: [temp_list[i], result[i][j]]})
                        }
                        else if (result[i][j].ma_san_bay_den != temp_list[i].ma_san_bay_di) { // Nếu chưa đến đích thì ghi vào mảng tạm để tìm tiếp
                            result[i][j].max_con_time = parseFloat(data.max_con_time)
                            result[i][j].time_forward = parseFloat(data.min_con_time)
                            temp_list1.push(result[i][j])
                            temp_list2.push([temp_list[i], result[i][j]])
                        }
                    }
                }

            }).then(function() { // Tim các chuyến bay 2 điểm dừng
                return q.all(temp_list1.map(flight_md.getAfterFlights))
            }).then(function(result) {
                for(var i = 0; i < temp_list1.length; i++) {
                    for(var j = 0; j < result[i].length; j++) {
                        if (result[i][j].ma_san_bay_den == data.to) { // Ghi nhận các chuyến bay 2 điểm dừng
                            if (fullFlights.length <= 3 * MAX_FLIGHT)
                            fullFlights.push({type: 2, flight: [...temp_list2[i], result[i][j]]})
                        }
                    }
                }
               
            }).then(function() {

                // Tính toán để ra quyết định
                var result
                
                if (params.time == null) { // Nếu không nhập thời điểm xuất phát
                    result = dSWithouStartTime.topsis({fullFlights: fullFlights}) // Gọi hàm Topsis để tính toán và sắp xếp chuyến bay
                    res.json(result)
                }
                else { // Nếu có sử dụng thời gian xuất phát
                    // try {
                        // Lấy thời gian và và cộng vào ngày đi
                        var hour = moment(params.time, 'HH:mm').get('h');
                        var minute = moment(params.time, 'HH:mm').get('m');
                        var dateTime = moment(data.date).add(hour, 'h').add(minute, 'm').toDate();

                        // Gọi hàm Topsis để tính toán và sắp xếp chuyến bay
                        result = dSUseStartTime.topsis({time: dateTime, fullFlights: fullFlights}) 
                        res.json(result)

                    // }
                    // catch {
                    //     res.json({ error: "Ngày giờ không hợp lệ" })
                    // }
                }

            })/* .catch(function (err) { // Nếu xảy ra lỗi
                var data = {
                    error: err
                }
                res.json(data)
            }) */
        }
    //}
 /*    catch {
        res.json({ error: "Đường dẫn không hợp lệ" })
    } */
})

module.exports = router;