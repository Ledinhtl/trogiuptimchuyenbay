var topsisScore = require('./helper')
var moment = require('moment')

const MAX_CON_TRIP = 3
const WORST_WAITING_TIME = 24 * 60; // Thoi gian phai cho doi den khi xuat phat lau nhat la 1 ngay

var p = { // priority
    gia_ve: 30,
    thoi_gian_bay: 9,
    thoi_diem_xuat_phat: 35,
    hanh_ly_xach_tay: 1,
    hanh_ly_ky_gui: 1,
    so_diem_dung: 5,
    suat_an: 1,
    giai_tri: 1,
    cong_sac: 1,
}
p.all = p.gia_ve + p.thoi_gian_bay + p.thoi_diem_xuat_phat + p.hanh_ly_xach_tay
+ p.hanh_ly_ky_gui + p.so_diem_dung + p.suat_an
+ p.giai_tri + p.cong_sac

const w = {
    gia_ve: p.gia_ve/p.all,
    thoi_gian_bay: p.thoi_gian_bay/p.all,
    thoi_diem_xuat_phat: p.thoi_diem_xuat_phat/p.all,
    hanh_ly_xach_tay: p.hanh_ly_xach_tay/p.all,
    hanh_ly_ky_gui: p.hanh_ly_ky_gui/p.all,
    so_diem_dung: p.so_diem_dung/p.all,
    suat_an: p.suat_an/p.all,
    giai_tri: p.giai_tri/p.all,
    cong_sac: p.cong_sac/p.all
}

function topsis(data) {
    var fullFlights = data.fullFlights
    var arrData = []
    var time = moment(data.time)
    var startTimeUser = time.get('h') * 60 + time.get('m')

    // Tinh toan cac gia tri ve kieu so
    for (var i = 0; i < fullFlights.length; i++) {
        var price = 0     // Tinh tong tien
        fullFlights[i].flight.forEach(function (flight) {
            price += flight.gia_ve
        })
        arrData.push([price])

        var toTalFlightTime = 0  // thoi gian bay ra phut
        fullFlights[i].flight.forEach(function (flight) {
            var flightTime = moment(flight.thoi_gian_bay, 'HH:mm:ss')
            var flightHour = flightTime.get('h')
            var flightMinute = flightTime.get('m')
            toTalFlightTime += (flightHour * 60 + flightMinute)
        })
        arrData[i].push(toTalFlightTime)

        var startTime = moment(fullFlights[i].flight[0].ngay_gio_di) // Thoi gian cho doi ra phut
        arrData[i].push(Math.abs(startTime.get('h') * 60 + startTime.get('m') - startTimeUser))

        var tongHanhLyXachTay = 0     // Hanh ly xach tay tinh trung binh
        fullFlights[i].flight.forEach(function (flight) {
            var hanhLyXachTay = flight.hanh_ly_xach_tay
            if (hanhLyXachTay == "" || hanhLyXachTay == null) {
                hanhLyXachTay = 0
            }
            else {
                hanhLyXachTay = parseInt(hanhLyXachTay)
            }
            tongHanhLyXachTay += hanhLyXachTay
        })
        arrData[i].push(tongHanhLyXachTay / fullFlights[i].flight.length)

        var tongHanhLyKyGui = 0     // Hanh ly ky gui tinh trung binh
        fullFlights[i].flight.forEach(function (flight) {
            var hanhLyKyGui = flight.hanh_ly_ky_gui
            if (hanhLyKyGui == "" || hanhLyKyGui == null) {
                hanhLyKyGui = 0
            }
            else {
                hanhLyKyGui = parseInt(hanhLyKyGui)
            }
            tongHanhLyKyGui += hanhLyKyGui
        })
        arrData[i].push(tongHanhLyKyGui / fullFlights[i].flight.length)

        arrData[i].push(fullFlights[i].type)     // So diem dung

        var tongSuatAn = 0     // Suat an tinh trung binh
        fullFlights[i].flight.forEach(function (flight) {
            var suatAn = flight.suat_an
            if (suatAn == "" || suatAn == null) {
                suatAn = 0
            }
            else {
                suatAn = 1
            }
            tongSuatAn += suatAn
        })
        arrData[i].push(tongSuatAn / fullFlights[i].flight.length)

        var tongGiaiTri = 0     // Giai Tri tinh trung binh
        fullFlights[i].flight.forEach(function (flight) {
            var giaiTri = flight.giai_tri
            if (giaiTri == "" || giaiTri == null) {
                giaiTri = 0
            }
            else {
                giaiTri = 1
            }
            tongGiaiTri += giaiTri
        })
        arrData[i].push(tongGiaiTri / fullFlights[i].flight.length) // Tinh trung binh

        var tongCongSac = 0     // Cong Sac
        fullFlights[i].flight.forEach(function (flight) {
            var congSac = flight.cong_sac
            if (congSac == "" || congSac == null) {
                congSac = 0
            }
            else {
                congSac = 1
            }
            tongCongSac += congSac
        })
        arrData[i].push(tongCongSac / fullFlights[i].flight.length) // Tinh trung binh

    }

    // Tim gia tri tot nhat / toi nhat

    var minPrice = arrData[0][0]
    var minFlightTime = arrData[0][1]
    var minWaitingTime = arrData[0][2]
    var maxHanhLyXachTay = arrData[0][3]
    var maxHanhLyKyGui = arrData[0][4]

    for (var i = 1; i < arrData.length; i++) {
        if (arrData[i][0] < minPrice) {
            minPrice = arrData[i][0]
        }
        if (arrData[i][1] < minFlightTime) {
            minFlightTime = arrData[i][1]
        }
        if (arrData[i][2] < minWaitingTime) {
            minWaitingTime = arrData[i][2]
        }
        if (arrData[i][3] > maxHanhLyXachTay) {
            maxHanhLyXachTay = arrData[i][3]
        }
        if (arrData[i][4] > maxHanhLyKyGui) {
            maxHanhLyKyGui = arrData[i][4]
        }
    }

    var subTimeWait = WORST_WAITING_TIME - minWaitingTime // Xu ly thoi diem xuat phat

    // Chuan hoa cac thuoc tinh
    for (var i = 0; i < arrData.length; i++) {
        arrData[i][0] = minPrice / arrData[i][0]
        arrData[i][1] = minFlightTime / arrData[i][1]
        arrData[i][2] = (WORST_WAITING_TIME - arrData[i][2]) / subTimeWait
        arrData[i][3] = arrData[i][3] / maxHanhLyXachTay
        arrData[i][4] = arrData[i][4] / maxHanhLyKyGui
        arrData[i][5] = (MAX_CON_TRIP - arrData[i][5]) / MAX_CON_TRIP
    }

    // Nhan cac trong so tuong ung

    for (var i = 0; i < arrData.length; i++) {
        arrData[i][0] *= w.gia_ve
        arrData[i][1] *= w.thoi_gian_bay
        arrData[i][2] *= w.thoi_diem_xuat_phat
        arrData[i][3] *= w.hanh_ly_xach_tay
        arrData[i][4] *= w.hanh_ly_ky_gui
        arrData[i][5] *= w.so_diem_dung
        arrData[i][6] *= w.suat_an
        arrData[i][7] *= w.giai_tri
        arrData[i][8] *= w.cong_sac
    }
    // Tim phuong an tot nhat aStar va te nhat aSubtract
    var aStar = [arrData[0][0], arrData[0][1], arrData[0][2], arrData[0][3], arrData[0][4], arrData[0][5], arrData[0][6], arrData[0][7], arrData[0][8]]
    var aSubt = [arrData[0][0], arrData[0][1], arrData[0][2], arrData[0][3], arrData[0][4], arrData[0][5], arrData[0][6], arrData[0][7], arrData[0][8]]
    for (var i = 1; i < arrData.length; i++) {
        if (arrData[i][0] > aStar[0]) aStar[0] = arrData[i][0]
        if (arrData[i][1] > aStar[1]) aStar[1] = arrData[i][1]
        if (arrData[i][2] > aStar[2]) aStar[2] = arrData[i][2]
        if (arrData[i][3] > aStar[3]) aStar[3] = arrData[i][3]
        if (arrData[i][4] > aStar[4]) aStar[4] = arrData[i][4]
        if (arrData[i][5] > aStar[5]) aStar[5] = arrData[i][5]
        if (arrData[i][6] > aStar[6]) aStar[6] = arrData[i][6]
        if (arrData[i][7] > aStar[7]) aStar[7] = arrData[i][7]
        if (arrData[i][8] > aStar[8]) aStar[8] = arrData[i][8]

        if (arrData[i][0] < aSubt[0]) aSubt[0] = arrData[i][0]
        if (arrData[i][1] < aSubt[1]) aSubt[1] = arrData[i][1]
        if (arrData[i][2] < aSubt[2]) aSubt[2] = arrData[i][2]
        if (arrData[i][3] < aSubt[3]) aSubt[3] = arrData[i][3]
        if (arrData[i][4] < aSubt[4]) aSubt[4] = arrData[i][4]
        if (arrData[i][5] < aSubt[5]) aSubt[5] = arrData[i][5]
        if (arrData[i][6] < aSubt[6]) aSubt[6] = arrData[i][6]
        if (arrData[i][7] < aSubt[7]) aSubt[7] = arrData[i][7]
        if (arrData[i][8] < aSubt[8]) aSubt[8] = arrData[i][8]
    }
    // Tim khoang cach den a * va a-
    var sStar = []
    var sSubt = []

    for (var i = 0; i < arrData.length; i++) {
        sStar.push(Math.sqrt((arrData[i][0] - aStar[0]) ** 2 + (arrData[i][1] - aStar[1]) ** 2 + (arrData[i][2] - aStar[2]) ** 2 + (arrData[i][3] - aStar[3]) ** 2 + 
        (arrData[i][4] - aStar[4]) ** 2 + (arrData[i][5] - aStar[5]) ** 2 + (arrData[i][6] - aStar[6]) ** 2 + (arrData[i][7] - aStar[7]) ** 2 + (arrData[i][8] - aStar[8]) ** 2))

        sSubt.push(Math.sqrt((arrData[i][0] - aSubt[0]) ** 2 + (arrData[i][1] - aSubt[1]) ** 2 + (arrData[i][2] - aSubt[2]) ** 2 + (arrData[i][3] - aSubt[3]) ** 2 + 
        (arrData[i][4] - aSubt[4]) ** 2 + (arrData[i][5] - aSubt[5]) ** 2 + (arrData[i][6] - aSubt[6]) ** 2 + (arrData[i][7] - aSubt[7]) ** 2 + (arrData[i][8] - aSubt[8]) ** 2))

        fullFlights[i].score = sSubt[i] / (sStar[i] + sSubt[i]) // Ghi diem TOPSIS cho cac phuong an
    }

    // Xep cac phuong an theo diem TOPSIS

    fullFlights.sort(topsisScore)

    return fullFlights

}

module.exports = {
    topsis: topsis
}