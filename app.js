var express = require('express');
var app = express();
var cors = require('cors')
var config = require('config')
var bodyParser = require("body-parser")
var controllers = require(__dirname + "/apps/controllers/index.js")
var port = config.get("server.port")

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(controllers);
app.use("/images", express.static(__dirname + "/logo_airlines"))
app.use(cors())

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.listen(port, function () {
    console.log("Server is running on port " + port);
});

app.get("/", function (req, res) {
    res.sendFile(__dirname + '/index.html')
})